@extends('layouts.app')

@section('content')
<div class="container mx-auto">
    <div class="flex justify-center items-center flex-col">
        <h1> Ludum Dare 40 </h1>
        <form class="panel md:w-1/2" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
            <div class="mb-4">
                <label for="username" class="frm-label">Username</label>
                <input id="username" type="text" class="frm-input{{ $errors->has('username') ? ' border-red' : '' }}" name="username" value="{{ old('username') }}" required autofocus>
                @if ($errors->has('username'))
                    <div class="text-red block font-bold mt-2">{{ $errors->first('username') }}</div>
                @endif
            </div>

            <div class="mb-4{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="frm-label">Password</label>
                <input id="password" type="password" class="frm-input{{ $errors->has('password') ? ' border-red' : '' }}" name="password" required>
                @if ($errors->has('password'))
                    <div class="text-red block font-bold mt-2">{{ $errors->first('password') }}</div>
                @endif
            </div>

            <div class="mb-4">
                <label for="password-confirm" class="frm-label">Confirm Password</label>
                <input id="password-confirm" type="password" class="frm-input" name="password_confirmation" required>
            </div>

            <button type="submit" class="btn btn-blue" type="button">
                Register
            </button>

        </form>
    </div>
</div>
@endsection
