<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="bg-grey-light">
    <div id="app">
        <nav class="bg-black p-1 w-full fixed pin-t">
            <div class="container mx-auto flex flex-col sm:flex-row justify-between items-center h-full">
                <a class="btn-nav" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                @if (Auth::guest())
                    <div class="flex">
                    <a class="btn-nav" href="{{ route('login') }}">Login</a>
                    <a class="btn-nav" href="{{ route('register') }}">Register</a>
                    </div>
                @else
                    <div>
                        <a class="btn-nav" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST">
                            {{ csrf_field() }}
                        </form>
                    </div>
                @endif
            </div>
        </nav>
        <div class="mt-24 sm:mt-18">
        @yield('content')
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
