<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drug extends Model
{
    protected $guarded = [];

    public function neighborhoods()
    {
        return $this->belongsToMany('App\Neighborhood')->withPivot('value');
    }
}
