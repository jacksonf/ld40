<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Neighborhood extends Model
{
    protected $guarded = [];

    public function drugs()
    {
        return $this->belongsToMany('App\Drug')->withPivot('value');
    }
}
