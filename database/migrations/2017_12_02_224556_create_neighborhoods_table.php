<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Neighborhood;

class CreateNeighborhoodsTable extends Migration
{
    public function up()
    {
        Schema::create('neighborhoods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        collect([
            'Flax Lane',
            'Meadow Avenue',
            'Terrace Way',
            'Providence Lane',
            'Liberty Road',
            'Moonlight Lane',
            'Summit Street',
            'Maple Street',
            'Parkview Lan',
            'Amber Boulevard',
            'Ranger Street',
            'Cathedral Way'
            ])
            ->each(function($name) {
                Neighborhood::create([
                    'name' => $name
                ]);
        });
        

    }

    public function down()
    {
        Schema::dropIfExists('neighborhoods');
    }
}
