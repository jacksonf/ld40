<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Neighborhood;
use App\Drug;

class CreateDrugsTable extends Migration
{
    public function up()
    {
        Schema::create('drugs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('price');
            $table->timestamps();
        });

        Schema::create('drug_neighborhood', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('drug_id')->unsigned();
            $table->foreign('drug_id')->references('id')->on('drugs')->onDelete('cascade');
            $table->integer('neighborhood_id')->unsigned();
            $table->foreign('neighborhood_id')->references('id')->on('neighborhoods')->onDelete('cascade');
            $table->integer('value');
        });

        $marijuana = Drug::create([
            'name'  => 'Marijuana',
            'price' => 20
        ]);
        $cocaine = Drug::create([
            'name'  => 'Cocaine',
            'price' => 20
        ]);
        $heroin = Drug::create([
            'name'  => 'Heroin',
            'price' => 20
        ]);
        $mdma = Drug::create([
            'name'  => 'MDMA',
            'price' => 20
        ]);
        $bath_salts = Drug::create([
            'name'  => 'Bath Salts',
            'price' => 20
        ]);

        Neighborhood::get()->each(function($neighborhood) use ($marijuana, $cocaine, $heroin, $mdma, $bath_salts) {
            $neighborhood->drugs()->attach($marijuana->id, ['value' => 100]);
            $neighborhood->drugs()->attach($cocaine->id, ['value' => 100]);
            $neighborhood->drugs()->attach($heroin->id, ['value' => 100]);
            $neighborhood->drugs()->attach($mdma->id, ['value' => 100]);
            $neighborhood->drugs()->attach($bath_salts->id, ['value' => 100]);
        });
    }

    public function down()
    {
        Schema::dropIfExists('drug_neighborhood');
        Schema::dropIfExists('drugs');
    }
}
